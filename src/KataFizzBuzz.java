
public class KataFizzBuzz {

	public static final String FIZZ = "Fizz";
	public static final String BUZZ = "Buzz";
	public static final int maxNumber = 100;

	public static void main(String[] args) {

		for (int i = 1; i <= maxNumber; i++) {
			System.out.println(getStringForNumber(i));
		}

	}

	public static String getStringForNumber(int n) {

		String result = null;

		if (n % 15 == 0) {
			result = FIZZ + BUZZ;
		} else if (n % 3 == 0) {
			result = FIZZ;
		} else if (n % 5 == 0) {
			result = BUZZ;
		} else {
			result = String.valueOf(n);
		}

		return result;

	}

}
