import static org.junit.Assert.*;

import org.junit.Test;

public class KataFizzBuzzTest {

	@Test
	public void test1() {
		assertEquals("1", KataFizzBuzz.getStringForNumber(1));
	}

	@Test
	public void test2() {
		assertEquals("2", KataFizzBuzz.getStringForNumber(2));
	}

	@Test
	public void test3() {
		assertEquals("Fizz", KataFizzBuzz.getStringForNumber(3));
	}

	@Test
	public void test4() {
		assertEquals("4", KataFizzBuzz.getStringForNumber(4));
	}

	@Test
	public void test5() {
		assertEquals("Buzz", KataFizzBuzz.getStringForNumber(5));
	}

	@Test
	public void test14() {
		assertEquals("14", KataFizzBuzz.getStringForNumber(14));
	}

	@Test
	public void test15() {
		assertEquals("FizzBuzz", KataFizzBuzz.getStringForNumber(15));
	}

	@Test
	public void test100() {
		assertEquals("Buzz", KataFizzBuzz.getStringForNumber(100));
	}

}
